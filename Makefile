.PHONY: all setup clean

all: setup stubs
clean: clean-stubs

setup:
ifneq (${CI}, true)
	git submodule update --init --recursive
endif
	poetry install

STUB_DIR = .mypy_stubs

STUB_PACKAGES := \
influxdb_client coloredlogs uvicorn tabulate \
pyocd cmsis_pack_manager \
pycyphal

stubs:
	poetry run stubgen -o ${STUB_DIR} $(addprefix -p ,${STUB_PACKAGES})

clean-stubs:
	rm -rf ${STUB_DIR}
