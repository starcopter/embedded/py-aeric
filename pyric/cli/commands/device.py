# Copyright (c) 2023 starcopter GmbH
# This software is distributed under the terms of the MIT License.
# Author: Lasse Fröhner <lasse@starcopter.com>

import pathlib
import re
from datetime import datetime
from functools import partial
from typing import Optional, Union

import click
from pyocd.core.helpers import ConnectHelper
from pyocd.core.memory_map import FlashRegion
from pyocd.core.soc_target import SoCTarget
from pyocd.flash.loader import MemoryLoader
from tabulate import tabulate

from ... import get_logger
from ...types.dcb import DCBBase, DeviceConfigurationV0, DeviceConfigurationV1, make_dcb
from ...types.device_id import STM32G4DeviceID
from ...types.image import AppImage, AppImageV3, Image, NativeImage, make_image
from ...types.versions import HardwareVersion
from .. import subgroup
from .image import discover_image

_logger = get_logger(__name__)
BOOTLOADER_START, BOOTLOADER_END = 0, 0x0600
DCB_START, DCB_END = BOOTLOADER_END, 0x0800
PAGE_SIZE = 0x0800


@subgroup()
def device() -> None:
    """Interact with devices directly connected via an on-chip debugging interface.

    The commands in this module provide methods to directly read from and write to a device's internal memory.

    Reading from a device currently only includes collecting information about installed software and configuration.
    Writing to a device may include flashing an image, provisioning a new device, or statically changing its configuration.
    Future functionality may include reading or writing option bytes or changing runtime configuration in the NVM block,
    although nothing of the sort has been implemented so far.
    """
    pass


@device.command()
@click.argument("name")
@click.argument("release")
@click.option("--date", default=None, metavar="DATE", help="device provisioning date override in ISO format")
@click.option("--uid", default=None, metavar="HEX", help="device UID override, formatted as hex string")
@click.option(
    "--bootloader",
    type=click.Path(exists=True, path_type=pathlib.Path),
    default=".",
    help="bootloader image (search) path",
)
@click.option(
    "--app",
    type=click.Path(exists=True, path_type=pathlib.Path),
    multiple=True,
    default=[],
    help="additional applications to load into to device, may be specified multiple times",
)
@click.option("--confirm/--no-confirm", default=True, help="Ask for confirmation before taking any action")
def provision(
    name: str,
    release: str,
    date: Optional[str],
    uid: Optional[str],
    bootloader: pathlib.Path,
    app: list[pathlib.Path],
    confirm: bool,
) -> None:
    """Provision a device with a bootloader and device-specific device configuration block (DCB).

    Provisioning a device consists of at least uploading a bootloader and a device configuration block. In a common
    scenario, also one or more additional applications (like the App Loader) will be programmed at the same time,
    since the immutable bootloader is only capable of loading applications, not receiving them over a bus.

    The device configuration block (DCB) includes the target platform name (like specified in uavcan.node.GetInfo.1.0,
    e.g. 'com.manufacturer.project.product'), hardware release (Altium-style char.uint8, e.g. 'A.1' or 'B.17'),
    provisioning date (microsecond resolution) and device UID (128-bit).
    NAME and RELEASE have to be specified by the user, while date and UID will be generated if omitted.

    \b
    If not specified by the user, the device UID will be constructed as follows:
    - the first 32 bits are the CRC32 of the name's manufacturer part, e.g. crc32("com.starcopter")
    - the next 32 bits are the CRC32 of the name's project-specific part, e.g. crc32("aeric.bms")
    - the third group of 32 bits is the timestamp in big-endian, e.g. 6219135c for 2022-02-25 18:35:24 CET
    - the final group contains a hash over the platform's chip ID (STM32 device ID register)

    This method of constructing a device UID has nice sorting behavior, while keeping at least some level of randomness.
    """
    target_release = HardwareVersion(release)
    if date is not None:
        date = datetime.fromisoformat(date)
    if uid is not None:
        uid = bytes.fromhex(uid)

    _logger.debug("resolving bootloader path %s", bootloader)
    bootloader_image: NativeImage = discover_image(bootloader, partial(make_image, base_class=NativeImage))
    if len(bootloader_image) > 0x0600:
        click.echo(f"{click.format_filename(bootloader_image.file)} is too large", err=True)
        return
    _logger.info("bootloader %s found", bootloader_image.description_dict)

    app_images: list[AppImageV3] = []
    for path in app:
        _logger.debug("resolving application path %s", path)
        image: AppImageV3 = discover_image(path, partial(make_image, base_class=AppImageV3))
        assert isinstance(image, AppImageV3)
        _logger.info("application %s found", image.description_dict)

        if not image.is_compatible_to(platform=name, release=target_release):
            _logger.warning(f"application {image!s} is incompatible with target platform and will be ignored")
            continue

        app_images.append(image)

    with ConnectHelper.session_with_chosen_probe() as session:
        target: SoCTarget = session.board.target
        loader = MemoryLoader(session, chip_erase="sector", smart_flash=True, trust_crc=False, keep_unwritten=False)

        device_id = STM32G4DeviceID.load_from_target(target)
        _logger.info(f"device ID {device_id!s} read from target")

        dcb = DeviceConfigurationV1.new(name, release, date, uid, device_id)
        _logger.info(f"about to write {dcb!s} to target")

        print_device_information(dcb, device_id)
        print(
            tabulate([format_application_details(img) for img in [bootloader_image] + app_images], headers="keys")
            + "\n"
        )

        flash = target.memory_map.get_boot_memory()
        assert flash.start == bootloader_image.offset, "Flash start has to match bootloader offset"

        if confirm:
            click.confirm(f"Provision target with {dcb!s} and {len(app_images)+1} images?", abort=True)

        loader.add_data(bootloader_image.offset, bootloader_image[:])
        loader.add_data(bootloader_image.offset + 0x600, dcb.pack())
        for image in app_images:
            loader.add_data(image.offset, image[:])

        target.reset_and_halt()
        loader.commit()


@device.command()
@click.argument("app", type=click.Path(exists=True, path_type=pathlib.Path), nargs=-1, required=True)
@click.option("--force", is_flag=True, help="Ignore compatibility and sanity checks")
def flash(app: tuple[pathlib.Path], force: bool) -> None:
    """Flash application images to a connected device.

    All images specified in this command's invocation will be uploaded to the connected device. Unless the
    --force option is selected, the command will silently ignore application images incompatible with the device.

    As with the image introspection commands, a partial path is sufficient to select an image.
    If a path is ambiguous, an interactive selection dialogue will be displayed.
    """
    images: set[AppImage] = {discover_image(path, partial(make_image, base_class=AppImage)) for path in app}

    with ConnectHelper.session_with_chosen_probe() as session:
        target: SoCTarget = session.board.target
        flash_region: FlashRegion = target.memory_map.get_boot_memory()
        dcb_bytes = bytes(target.read_memory_block8(flash_region.start + DCB_START, DCB_END - DCB_START))
        try:
            dcb = make_dcb(dcb_bytes)
        except ValueError:
            dcb = None
        device_id = STM32G4DeviceID.load_from_target(target)
        print_device_information(dcb, device_id)

        loader = MemoryLoader(session, chip_erase="sector", smart_flash=True, trust_crc=False, keep_unwritten=False)
        queue = list()

        for image in sorted(images, key=lambda image: image.offset):
            if (
                not force
                and isinstance(image, AppImageV3)
                and isinstance(dcb, (DeviceConfigurationV0, DeviceConfigurationV1))
                and not image.is_compatible_to(dcb.name, dcb.release)
            ):
                click.echo(f"{image!s} is incompatible with target platform and will be ignored", err=True)
                continue

            loader.add_data(image.offset, image[:])
            queue.append(image)

        print(tabulate([format_application_details(img) for img in queue], headers="keys") + "\n")

        print(f"Uploading {len(queue)} images ({sum(len(img) for img in queue) / 1024:.1f} KiB)...")
        target.reset_and_halt()
        loader.commit()
        target.resume()


@device.command()
def inspect() -> None:
    """Inspect a connected device and display a summary.

    This command reads the contents of a connected device's flash memory, and will try to detect applications and
    configuration in that memory dump. It is capable of detecting a (single) natively executable bootloader at the
    beginning of the memory region, decoding the device configuration block at its fixed address, and collecting all
    remaining application images in the space that follows.
    A relevant section of the collected metadata will be displayed in a tabular overview.

    The logic behind this command is hard-coded to the application structure and memory layout expected in (current)
    starcopter embedded systems, and is unlikely to work with other devices.
    """

    with ConnectHelper.session_with_chosen_probe() as session:
        target: SoCTarget = session.board.target
        content = tuple(scan_memory(target).values())
        device_id = STM32G4DeviceID.load_from_target(target)

        for obj in content:
            if isinstance(obj, (DeviceConfigurationV0, DeviceConfigurationV1)):
                dcb = obj
                break
        else:
            dcb = None

        print_device_information(dcb, device_id)
        print(tabulate([format_application_details(obj) for obj in content if isinstance(obj, Image)], headers="keys"))


def print_device_information(
    dcb: Union[DeviceConfigurationV0, DeviceConfigurationV1, None] = None, device_id: Optional[STM32G4DeviceID] = None
) -> None:
    description = dict()
    if dcb is not None:
        description |= {"DCB type": dcb.__class__.__name__, "device": dcb.name, "release": str(dcb.release)}
        if isinstance(dcb, DeviceConfigurationV1):
            description |= {"date": dcb.date_string, "uid": dcb.uid.hex()}
    device_id_string = re.match(r"\w+\((.+)\)", str(device_id)).group(1)
    description |= {"chip": device_id_string}

    print(tabulate(description.items()) + "\n")


def format_application_details(image: Image) -> dict[str, str]:
    details = {
        "Address Range": f"0x{image.offset:08x}..0x{image.offset+len(image):08x}",
        "Type": image.__class__.__name__,
        "Name": image.name,
        "Version": str(image.version),
        "GNU Build ID": image.build_id,
        "Git SHA": image.git_sha[:8] if isinstance(image, AppImageV3) else "",
        "Timestamp": image.build_time_string,
    }
    return details


def scan_memory(target: SoCTarget) -> dict[int, Union[Image, DCBBase]]:
    flash_region: FlashRegion = target.memory_map.get_boot_memory()
    flash = bytes(target.read_memory_block8(flash_region.start, flash_region.length))
    stuff: dict[int, Union[Image, DCBBase]] = dict()

    def addr(offset: int) -> int:
        return offset + flash_region.start

    try:
        bootloader = NativeImage(flash[BOOTLOADER_START:BOOTLOADER_END])
        stuff[addr(BOOTLOADER_START)] = bootloader
        _logger.info(f"Bootloader found at 0x{addr(BOOTLOADER_START):08x}: {bootloader.description_dict}")
    except ValueError:
        _logger.debug(f"no bootloader found at 0x{addr(BOOTLOADER_START):08x}")

    try:
        dcb = make_dcb(flash[DCB_START:DCB_END])
        stuff[addr(DCB_START)] = dcb
        _logger.info(f"DCB found at 0x{addr(DCB_START):08x}: {dcb!s}")
    except ValueError:
        _logger.debug(f"no DCB found at 0x{addr(DCB_START):08x}")

    start = DCB_END

    while start < len(flash):
        try:
            image: AppImage = make_image(data=flash[start:], base_class=AppImage)
            stuff[addr(start)] = image
            _logger.info(f"{image.__class__.__name__} found at 0x{addr(start):08x}: {image.description_dict}")
            start += len(image)
            # round up
            start += PAGE_SIZE - (len(image) % PAGE_SIZE)
        except ValueError:
            start += PAGE_SIZE

    return stuff
