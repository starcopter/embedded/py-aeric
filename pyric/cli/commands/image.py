# Copyright (c) 2023 starcopter GmbH
# This software is distributed under the terms of the MIT License.
# Author: Lasse Fröhner <lasse@starcopter.com>

import pathlib
from collections.abc import Iterable
from functools import partial
from typing import Callable, Generator, List, Optional, Sequence, Union

import click
from tabulate import tabulate

from ... import get_logger
from ...types import AppImage, Image, make_image
from .. import subgroup

_logger = get_logger(__name__)


@subgroup()
def image() -> None:
    """Interact with compiled images, both native and with application header.

    The commands in this module provide easy access to common image-related tasks. This covers both native images
    (e.g. first-level bootloaders) and images with different custom application headers. The type of a specific image
    will be automatically detected, but depending on the image different information will be available.
    For example, all images of type AppImage will have a checksum and will be validatable (and patchable), but only
    an AppImageV3 has an application name and version information embedded.
    """
    pass


@image.command("list")
@click.argument("path", type=click.Path(exists=True, file_okay=False, path_type=pathlib.Path), default=".")
def list_images(path: pathlib.Path):
    """Recursively discover all images in a directory and display them in a tabular overview.

    This command takes a single existing directory path as argument and searches for image files in the directory's
    subtree. The list of discovered images is displayed in a tabular overview.
    """
    images = find_images(path)
    _logger.debug("%i images discovered under %s", len(images), path)
    print_image_list(images, reference_path=path)


@image.command()
@click.argument("path", type=click.Path(exists=True, path_type=pathlib.Path), default=".")
def info(path: pathlib.Path) -> None:
    """Display all available information on a specific image.

    The 'info' command takes an image path as argument and displays all available image metadata.
    The level of detail depends on the image type, as an AppImageV3 has more information embedded than a NativeImage.

    \b
    The PATH argument may be either a file path or a directory path:
    - if PATH is a file, information on that file will be displayed.
    - if PATH is a directory containing only one valid image, information on that image will be displayed.
    - if PATH is a directory containing multiple images, an image has to be selected in an interactive dialog.
    """
    image = discover_image(path, make_image)
    image.describe()


@image.command()
@click.argument("path", type=click.Path(exists=True, path_type=pathlib.Path), default=".")
def patch(path: pathlib.Path) -> None:
    """Calculate an AppImage's Header and Image CRC and write them into the binary file.

    The 'patch' command is intended to be used e.g. in a build pipeline to patch an image after compilation.
    Only images of type AppImage (or subtypes) can be patched.

    \b
    The PATH argument may be either a file path or a directory path:
    - if PATH is a file, that file will be patched.
    - if PATH is a directory containing only one valid application image, that image will be patched.
    - if PATH is a directory containing multiple AppImages, an image has to be selected interactively.
    """
    image: AppImage = discover_image(path, partial(make_image, base_class=AppImage))
    image.patch()
    click.echo(f"CRCs {image.header_crc:08x} and {image.image_crc:08x} written to {image.file!s}")


def find_images(root: pathlib.Path, factory: Callable[[pathlib.Path], Image] = make_image) -> List[Image]:
    def valid_images_in_subtree() -> Generator[Image, None, None]:
        for path in root.glob("**/*.bin"):
            try:
                yield factory(path)
            except (ValueError, FileNotFoundError):
                continue

    return sorted(
        set(valid_images_in_subtree()),
        key=lambda img: (
            img.offset,
            len(img.file.parts) if img.file is not None else 0,
            str(img.file),
            *img.version,
        ),
    )


def print_image_list(
    images: Sequence[Image], reference_path: Optional[pathlib.Path] = None, indices: Union[bool, Iterable[int]] = False
) -> None:
    def fmt_path(path: pathlib.Path) -> str:
        return str(path) if reference_path is None else str(path.relative_to(reference_path.resolve()))

    image_details = [
        {
            "Type": img.__class__.__name__,
            "File Path": fmt_path(img.file),
            "Application": img.name if isinstance(img, AppImage) else "",
            "Version": str(img.version) if img.version else "",
            "Build Time": img.build_time_string,
            "GNU Build ID": img.build_id,
        }
        for img in images
    ]

    print(tabulate(image_details, headers="keys", tablefmt="simple", showindex=indices) + "\n")


def discover_image(path: pathlib.Path, factory: Callable[[pathlib.Path], Image] = make_image) -> Image:
    if path.is_file():
        try:
            return factory(path)
        except ValueError:
            raise click.FileError(str(path), "not a valid image")

    if not path.is_dir():
        raise click.FileError(str(path), "path has to be either a file or a directory")

    images = find_images(path, factory)
    if not images:
        raise click.FileError(str(path), "no valid image found in subtree")

    if len(images) == 1:
        return images[0]

    print_image_list(images, reference_path=path, indices=range(1, len(images) + 1))

    while True:
        choice = input("choose image: ")
        try:
            index = int(choice) - 1
            if index < 0:
                raise IndexError
            image = images[index]
            print()  # insert an empty line
            return image
        except (ValueError, IndexError):
            print(f"Please specify an index between 1 and {len(images)}.")
            continue
