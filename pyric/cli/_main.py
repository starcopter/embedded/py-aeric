# Copyright (c) 2023 starcopter GmbH
# This software is distributed under the terms of the MIT License.
# Author: Lasse Fröhner <lasse@starcopter.com>

import logging
from shutil import get_terminal_size
from typing import Any, Callable, Optional

import click

import pyric

_logger = pyric.get_logger("pyric")
_LOG_FORMAT = "%(asctime)s %(levelname)-3.3s %(name)s: %(message)s"


# borrowed from https://github.com/OpenCyphal/yakut/blob/main/yakut/main.py
class AbbreviatedGroup(click.Group):
    def get_command(self, ctx: click.Context, cmd_name: str) -> Optional[click.Command]:
        cmd_name = cmd_name.replace("_", "-")
        rv = click.Group.get_command(self, ctx, cmd_name)
        if rv is not None:
            return rv
        matches = [x for x in self.list_commands(ctx) if x.startswith(cmd_name)]
        if not matches:
            return None
        if len(matches) == 1:
            return click.Group.get_command(self, ctx, matches[0])
        ctx.fail(f"Abbreviated command {cmd_name!r} is ambiguous. Possible matches: {list(matches)}")


@click.command(
    cls=AbbreviatedGroup,
    context_settings={"max_content_width": get_terminal_size()[0], "auto_envvar_prefix": "PYRIC"},
)
@click.version_option(version=pyric.__version__)
@click.option("--verbose", "-v", count=True, help="Emit verbose log messages. Specify twice for extra verbosity.")
@click.pass_context
def main(ctx: click.Context, verbose: int) -> None:
    _configure_logging(verbose)
    pass


subgroup: Callable[..., Callable[..., Any]] = main.group  # type: ignore


def _configure_logging(verbosity_level: int) -> None:
    log_level = {
        0: logging.WARNING,
        1: logging.INFO,
        2: logging.DEBUG,
    }.get(verbosity_level or 0, logging.DEBUG)

    logging.root.setLevel(log_level)

    import coloredlogs  # type: ignore

    # The level spec applies to the handler, not the root logger! This is different from basicConfig().
    coloredlogs.install(level=log_level, fmt=_LOG_FORMAT)
