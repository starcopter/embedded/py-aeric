import struct
from dataclasses import dataclass
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from pyocd.core.soc_target import SoCTarget


@dataclass()
class STM32G4DeviceID:
    ADDRESS = 0x1FFF7590
    SIZE = 3  # 96 bits

    lot: str
    wafer: int
    x: int
    y: int

    @classmethod
    def from_bytes(cls, raw: bytes):
        lot, wafer, x, y = struct.unpack(">7sBHH", raw)
        self = cls(lot.decode("ascii").strip(), wafer, x, y)
        assert self.pack() == raw
        return self

    @classmethod
    def load_from_target(cls, target: "SoCTarget"):
        assert target.part_number.startswith("STM32G4")
        target.reset_and_halt()
        device_id_little_endian = target.read_memory_block32(cls.ADDRESS, cls.SIZE)
        device_id = struct.pack(">LLL", *tuple(reversed(device_id_little_endian)))
        return cls.from_bytes(device_id)

    def pack(self) -> bytes:
        blot = self.lot.rjust(7).encode("ascii")
        return struct.pack(">7sBHH", blot, self.wafer, self.x, self.y)
