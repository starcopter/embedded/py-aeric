# Copyright (c) 2023 starcopter GmbH
# This software is distributed under the terms of the MIT License.
# Author: Lasse Fröhner <lasse@starcopter.com>

import re
import struct
from functools import total_ordering
from typing import Any, Tuple, overload

from pydantic import BaseModel, Field, root_validator, validator  # pylint: disable=no-name-in-module


@total_ordering
class _BaseVersion(BaseModel):
    """Hard- and SoftwareVersion base class."""

    # pylint: disable=no-self-argument,no-self-use
    @root_validator(pre=True)
    def _drop_none(cls, values: dict[str, Any]) -> dict[str, Any]:
        return {k: v for k, v in values.items() if v is not None}

    @property
    def _sort_key(self) -> Tuple:
        return tuple(self.dict().values())

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, self.__class__):
            raise TypeError(f"{self.__class__.__name__} can only be compared to its own kind")
        return self._sort_key == __o._sort_key

    def __lt__(self, __o: object) -> bool:
        if not isinstance(__o, self.__class__):
            raise TypeError(f"{self.__class__.__name__} can only be compared to its own kind")
        return self._sort_key < __o._sort_key

    def __hash__(self) -> int:
        return hash(self._sort_key)


class SoftwareVersion(_BaseVersion):
    """Python Representation of the following C construct:

    ```c
    union SoftwareVersion {
        struct {
            const uint8_t commits;  // Git Commits after Version Tag, see `git describe`, capped at 255
            const uint8_t patch;    // Semantic Versioning
            const uint8_t minor;    // Semantic Versioning
            const uint8_t major;    // Semantic Versioning
        };
        uint32_t u32;  // sort accessor
    };
    ```

    In addition to the above described fields, this object describes the dirty-ness of an underlying git repository.
    """

    major: int = Field(ge=0, lt=256)
    minor: int = Field(ge=0, lt=256)
    patch: int = Field(default=0, ge=0, lt=256)
    commits: int = Field(default=0, ge=0, lt=256)
    dirty: bool = False

    # pylint: disable=no-self-argument,no-self-use,invalid-name
    @validator("dirty", pre=True)
    def _cast_bool(cls, v: Any) -> bool:
        return bool(v)

    @overload
    def __init__(self, /, *, major: int, minor: int, patch: int = 0, commits: int = 0, dirty: bool = False) -> None:
        """Create SoftwareVersion explicitly from version components.

        As this class is pydantic-backed, types will be silently converted under the hood.

        Examples:
            >>> SoftwareVersion(major=0, minor=1)
            SoftwareVersion(major=0, minor=1, patch=0, commits=0, dirty=False)
            >>> SoftwareVersion(major=0, minor="1", dirty="*")
            SoftwareVersion(major=0, minor=1, patch=0, commits=0, dirty=True)
        """

    @overload
    def __init__(self, version_string: str, /) -> None:
        """Create SoftwareVersion from a version string.

        Examples:
            >>> SoftwareVersion("0.3")
            SoftwareVersion(major=0, minor=3, patch=0, commits=0, dirty=False)
            >>> SoftwareVersion("v0.1.1-17*")
            SoftwareVersion(major=0, minor=1, patch=1, commits=17, dirty=True)
            >>> SoftwareVersion("v0.1.3-3-g420848d-dirty")
            SoftwareVersion(major=0, minor=1, patch=3, commits=3, dirty=True)
        """

    @overload
    def __init__(self, packed_data: bytes, /, *, dirty: bool = False) -> None:
        """Create SoftwareVersion from a packed struct.

        Examples:
            >>> SoftwareVersion(b'\\x11\\x01\\x01\\x00')
            SoftwareVersion(major=0, minor=1, patch=1, commits=17, dirty=False)
            >>> SoftwareVersion(b'\x03\x03\x01\x00', dirty=1)
            SoftwareVersion(major=0, minor=1, patch=3, commits=3, dirty=True)

            >>> import struct
            >>> SoftwareVersion(struct.pack("<I", 0x01020c))
            SoftwareVersion(major=0, minor=1, patch=2, commits=12, dirty=False)
        """

    def __init__(self, _arg=..., /, **data) -> None:
        if isinstance(_arg, str):
            match = re.match(
                r"v?(?P<major>\d+)\.(?P<minor>\d+)(\.(?P<patch>\d+))?"
                r"(-(?P<commits>\d+))?"
                r"(-g(?P<sha>[0-9a-f]+))?"
                r"(?P<dirty>-dirty|\*)?",
                _arg,
                re.I,
            )
            if not match:
                raise ValueError(f"version string not understood: {_arg!r}")
            data |= match.groupdict()
        elif isinstance(_arg, bytes):
            commits, patch, minor, major = struct.unpack("BBBB", _arg)
            data |= {"commits": commits, "patch": patch, "minor": minor, "major": major}

        super().__init__(**data)

    def pack(self) -> bytes:
        """Pack SoftwareVersion into Little-Endian binary representation.

        Attention:
            The `dirty` attribute is omitted in the packed representation.
        """
        return struct.pack("BBBB", self.commits, self.patch, self.minor, self.major)

    @property
    def u32(self) -> int:
        """`uint32_t` representation of packed struct."""
        return struct.unpack("<I", self.pack())[0]

    def __str__(self) -> str:
        version_string = f"v{self.major}.{self.minor}.{self.patch}"
        if self.commits or self.dirty:
            version_string += f"-{self.commits}"
        if self.dirty:
            version_string += "*"
        return version_string

    def __bool__(self) -> bool:
        return self != SoftwareVersion(major=0, minor=0)


class HardwareVersion(_BaseVersion):
    """
    Python Representation of the following C construct:

    ```c
    union HardwareVersion {
        struct {
            uint8_t minor;  // 0 through 255
            char    major;  // 'A' through 'Z'
        };
        uint16_t u16;  // sort accessor, A.0 (16640, 0x4100) through Z.255 (23295, 0x5aff)
    };
    ```
    """

    major: str = Field(default="0", regex=r"^0|[A-Z]$")
    minor: int = Field(default=0, ge=0, lt=256)

    @overload
    def __init__(self, /, *, major: str = "0", minor: int = 0) -> None:
        """Create HardwareVersion explicitly from major and minor version.

        Examples:
            >>> HardwareVersion(major="A", minor=1)
            HardwareVersion(major='A', minor=1)
            >>> HardwareVersion(major="B", minor="17")
            HardwareVersion(major='B', minor=17)
        """

    @overload
    def __init__(self, version_string: str, /) -> None:
        """Create HardwareVersion from a version string, e.g. `'A.1'` or `'B.17'`.

        Examples:
            >>> HardwareVersion("A.3")
            HardwareVersion(major='A', minor=3)
            >>> HardwareVersion("B.17")
            HardwareVersion(major='B', minor=17)
            >>> HardwareVersion()
            HardwareVersion(major='0', minor=0)
        """

    @overload
    def __init__(self, packed_data: bytes, /) -> None:
        """Create HardwareVersion from a packed struct, e.g. `b'\\x01A'` or `b'\\x11B'`.

        Examples:
            >>> HardwareVersion(b"\\x01A")
            HardwareVersion(major='A', minor=1)

            >>> import struct
            >>> HardwareVersion(struct.pack("<H", 0x4211))
            HardwareVersion(major='B', minor=17)
        """

    def __init__(self, _arg=None, /, **data) -> None:
        if isinstance(_arg, str):
            match = re.match(r"(?P<major>[A-Z])\.(?P<minor>\d+)", _arg)
            if not match:
                raise ValueError(f"version string not understood: {_arg!r}")
            data |= match.groupdict()
        elif isinstance(_arg, bytes):
            raw_major: bytes
            minor, raw_major = struct.unpack("Bc", _arg)
            data |= {"major": raw_major.decode(), "minor": minor}

        super().__init__(**data)

    def pack(self) -> bytes:
        """Pack HardwareVersion into Little-Endian binary representation."""
        return struct.pack("Bc", self.minor, self.major.encode("ascii"))

    @property
    def u16(self) -> int:
        """`uint16_t` representation of packed struct."""
        return struct.unpack("<H", self.pack())[0]

    def __str__(self) -> str:
        return f"{self.major}.{self.minor}"

    def __bool__(self) -> bool:
        return self != HardwareVersion()
