# Copyright (c) 2023 starcopter GmbH
# This software is distributed under the terms of the MIT License.
# Author: Lasse Fröhner <lasse@starcopter.com>

import binascii
import struct
import subprocess
from abc import abstractmethod
from datetime import datetime
from pathlib import Path
from typing import Any, Dict, Iterable, Optional, Type, Union
from zoneinfo import ZoneInfo

from .versions import HardwareVersion, SoftwareVersion

Address = int


class BinaryData:
    """Abstract binary data container, optionally backed by a file."""

    file: Optional[Path] = None

    @classmethod
    def from_file(cls: Type["BinaryData"], file: Union[str, Path]):
        file = Path(file).resolve()
        if not file.is_file():
            raise FileNotFoundError(f"{file!s} not found")
        instance = cls(file.read_bytes())
        instance.file = file
        return instance

    def __init__(self, data: Union[bytes, bytearray, Iterable[int]]) -> None:
        self._data = bytes(data)
        self.__post_init__()

    @abstractmethod
    def __post_init__(self) -> None:
        pass

    def commit(self) -> None:
        if self.file is None:
            raise AttributeError("This container is not backed by a file.")
        self.file.write_bytes(self._data)

    def __len__(self) -> int:
        return len(self._data)

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, self.__class__):
            return False
        return self._data == __o._data

    def __getitem__(self, __s: Union[int, slice]) -> bytes:
        if isinstance(__s, int):
            __s = slice(__s, __s + 1)
        return self._data[__s]

    def __setitem__(self, __s: Union[int, slice], __x: Union[bytes, bytearray, Iterable[int]]) -> None:
        mutable_data = bytearray(self._data)
        mutable_data[__s] = __x
        if len(mutable_data) != len(self._data):
            raise ValueError("value length must match slice length")
        self._data = bytes(mutable_data)

    def __hash__(self):
        return hash(self._data)

    def __repr__(self) -> str:
        if self.file is not None:
            return f"{self.__class__.__name__}.from_file({self.file!s})"
        return f"{self.__class__.__name__}({bytes(self)!r})"


class Image(BinaryData):
    FLASH_START: Address = 0x08000000
    FLASH_SIZE_MAX = 1024 * 1024  # at most 1 MiB
    RAM_START: Address = 0x20000000
    RAM_SIZE_MAX = 1024 * 1024  # at most 1 MiB
    VT_ALIGN = 512

    # static defaults, overridden in subclasses
    offset = FLASH_START
    version = SoftwareVersion("v0.0")
    clean_build = True
    git_sha = ""
    is_native_image = False
    is_app_image = False
    is_valid = True

    @property
    def build_time(self) -> datetime:
        timestamp = self.file.stat().st_mtime if self.file is not None else 0.0
        return datetime.fromtimestamp(timestamp)

    @property
    def build_time_string(self) -> str:
        if self.build_time.timestamp() == 0:
            return ""
        prefix = "after " if not self.clean_build else ""
        return prefix + self.build_time.astimezone().strftime("%Y-%m-%d %H:%M:%S %Z")

    @property
    def build_id(self) -> str:
        BUILD_ID_SIZE = 20
        BUILD_ID_HEADER = struct.pack("<LLL4s", 4, BUILD_ID_SIZE, 3, b"GNU\0")
        build_id_header_offset = self[:].find(BUILD_ID_HEADER)
        if build_id_header_offset < 0:
            return ""  # none found
        offset = build_id_header_offset + len(BUILD_ID_HEADER)
        raw_build_id = self[offset : offset + BUILD_ID_SIZE]
        return bytes(struct.unpack("20B", raw_build_id)).hex()

    @property
    def name(self) -> str:
        return self.file.name if self.file is not None else ""

    def __str__(self) -> str:
        if self.file is not None:
            return self.file.name
        if len(self) > 32:
            return self[:32].hex(" ", -4) + " ..."
        return self[:].hex(" ", -4)

    @property
    def description_dict(self) -> Dict[str, Any]:
        return {
            "file name": str(self.file) if self.file is not None else "",
            "type": f"{self.__class__.__name__}",
            "Build ID": self.build_id,
            "build date": self.build_time_string,
            "offset": f"0x{self.offset:08x} ({len(self) / 1024 :.1f} KiB)",
        }

    def describe(self) -> None:
        """print image metadata to stdout"""
        description = self.description_dict
        max_key_length = max(map(len, description.keys()))
        row_format_string = f"{{:>{max_key_length}s}}: {{}}"
        print("\n".join(row_format_string.format(key, val) for key, val in description.items()) + "\n")

    @property
    def root_dir(self) -> Path:
        if self.file is None:
            return Path(".")
        try:
            root_dir = subprocess.run(
                ["git", "rev-parse", "--show-toplevel"], cwd=self.file.parent, capture_output=True, text=True
            ).stdout.strip()
            return Path(root_dir)
        except:
            # assume a `project_root/build/target.bin` structure
            return self.file.parent.parent


class NativeImage(Image):
    @property
    def stack_top(self) -> Address:
        return struct.unpack("<L", self[0:4])[0]

    @property
    def entrypoint(self) -> Address:
        return struct.unpack("<L", self[4:8])[0] & ~1

    @property
    def is_native_image(self) -> bool:
        return (
            self.RAM_START < self.stack_top <= self.RAM_START + self.RAM_SIZE_MAX
            and self.FLASH_START < self.entrypoint <= self.FLASH_START + self.FLASH_SIZE_MAX
            and self.stack_top != AppImage.MAGIC
        )

    def __post_init__(self) -> None:
        if not self.is_native_image:
            name = self.file.name if self.file is not None else f"'{self[:8].hex()}'..."
            raise ValueError(f"{name} seems not to be a native image")

    @property
    def description_dict(self) -> Dict[str, Any]:
        return {
            "file name": str(self.file) if self.file is not None else "",
            "type": f"{self.__class__.__name__}",
            "Build ID": self.build_id,
            "build date": self.build_time_string,
            "address range": f"0x{self.offset:08x}..0x{self.offset+len(self):08x} ({len(self) / 1024 :.1f} KiB)",
            "vector table": f"0x{self.offset:08x}",
            "stack top": f"0x{self.stack_top:08x}",
            "entrypoint": f"0x{self.entrypoint:08x}",
        }


class AppImage(Image):
    MAGIC = b"\x7fsc0"
    HEADER_VERSION = None

    @property
    def is_app_image(self) -> bool:
        length_valid = self.file is None or len(self) == self.image_end - self.header_start
        return self.image_magic == self.MAGIC and self.is_vector_table_aligned and length_valid

    def __post_init__(self) -> None:
        name = self.file.name if self.file is not None else f"'{self[:8].hex()}'..."
        if not self.is_app_image:
            raise ValueError(f"{name} seems not to be an application image")
        if self.HEADER_VERSION is not None and self.header_version != self.HEADER_VERSION:
            raise ValueError(
                f"{name} has the wrong header version: expected {self.HEADER_VERSION}, got {self.header_version}"
            )
        if self.file is None:
            length = self.image_end - self.header_start
            # crop image
            self._data = self._data[:length]

    @property
    def image_magic(self) -> bytes:
        return self[:4]

    @property
    def header_start(self) -> Address:
        return struct.unpack("<L", self[4:8])[0]

    @property
    def vector_table(self) -> Address:
        return struct.unpack("<L", self[8:12])[0]

    @property
    def is_vector_table_aligned(self) -> bool:
        return not (self.vector_table & (self.VT_ALIGN - 1))

    @property
    def stack_top(self) -> Address:
        offset = self.vector_table - self.header_start
        return struct.unpack("<L", self[offset : offset + 4])[0]

    @property
    def entrypoint(self) -> Address:
        offset = self.vector_table - self.header_start + 4
        return struct.unpack("<L", self[offset : offset + 4])[0] & ~1

    @property
    def header_size(self) -> int:
        return struct.unpack("<L", self[12:16])[0]

    @property
    def image_end(self) -> Address:
        return struct.unpack("<L", self[16:20])[0]

    @property
    def header_version(self) -> int:
        return struct.unpack("B", self[20])[0]

    @property
    def header_crc(self) -> int:
        return struct.unpack("<L", self[self.header_size - 4 : self.header_size])[0]

    @header_crc.setter
    def header_crc(self, crc: int) -> None:
        self[self.header_size - 4 : self.header_size] = struct.pack("<L", crc)
        self.commit()

    @property
    def image_crc(self) -> int:
        return struct.unpack("<L", self[-4:])[0]

    @image_crc.setter
    def image_crc(self, crc: int) -> None:
        self[-4:] = struct.pack("<L", crc)
        self.commit()

    def calculate_header_crc(self) -> int:
        return binascii.crc32(self[: self.header_size - 4])

    def calculate_image_crc(self) -> int:
        return binascii.crc32(self[:-4])

    def validate(self, validate_checksums: bool = True) -> None:
        if self.image_magic != self.MAGIC:
            raise ValueError(f"Unsupported Image. Expected {self.MAGIC}, got {self.image_magic}.")
        if not self.is_vector_table_aligned:
            raise ValueError(f"Vector table at 0x{self.vector_table:08x} not aligned to {self.VT_ALIGN}-Byte boundary.")
        if len(self) != self.image_end - self.header_start:
            raise ValueError(f"Image length mismatch: expected {self.image_end - self.header_start}, got {len(self)}")
        if not validate_checksums:
            return
        calculated_header_crc = self.calculate_header_crc()
        if self.header_crc != calculated_header_crc:
            raise ValueError(f"header CRC mismatch: expected {self.header_crc:08x}, got {calculated_header_crc:08x}")
        calculated_image_crc = self.calculate_image_crc()
        if self.image_crc != calculated_image_crc:
            raise ValueError(f"image CRC mismatch: expected {self.image_crc:08x}, got {calculated_image_crc:08x}")

    def patch(self) -> None:
        self.validate(False)
        self.header_crc = self.calculate_header_crc()
        self.image_crc = self.calculate_image_crc()

    @property
    def is_valid(self) -> bool:
        try:
            self.validate()
            return True
        except ValueError:
            return False

    @property
    def offset(self) -> int:
        if not self.is_valid:
            # assume we've got a bootloader or something
            return self.FLASH_START
        return self.header_start

    @property
    def description_dict(self) -> Dict[str, Any]:
        return {
            "file name": str(self.file) if self.file is not None else "",
            "type": f"{self.__class__.__name__}, {'valid' if self.is_valid else 'invalid'}",
            "checksums": f"Header {self.header_crc:08x}, Image {self.image_crc:08x}",
            "Build ID": self.build_id,
            "build date": self.build_time_string,
            "address range": f"0x{self.header_start:08x}..0x{self.image_end:08x} ({len(self) / 1024 :.1f} KiB)",
            "vector table": f"0x{self.vector_table:08x}",
            "stack top": f"0x{self.stack_top:08x}",
            "entrypoint": f"0x{self.entrypoint:08x}",
        }


class AppImageV3(AppImage):
    HEADER_SIZE = 120
    HEADER_VERSION = 3

    FLAG_CLEAN_BUILD = 1

    @property
    def flags(self) -> int:
        return struct.unpack("H", self[22:24])[0]

    @property
    def clean_build(self) -> bool:
        return bool(self.flags & self.FLAG_CLEAN_BUILD)

    @property
    def version(self) -> "SoftwareVersion":
        version = SoftwareVersion(self[24:28])
        version.dirty = not self.clean_build
        return version

    @property
    def min_upgrade_from(self) -> "SoftwareVersion":
        return SoftwareVersion(self[28:32])

    @property
    def max_downgrade_from(self) -> "SoftwareVersion":
        return SoftwareVersion(self[32:36])

    @property
    def target_hardware(self) -> "HardwareVersion":
        return HardwareVersion(self[36:38])

    @property
    def min_hardware(self) -> "HardwareVersion":
        return HardwareVersion(self[38:40])

    @property
    def build_time(self) -> datetime:
        timestamp = struct.unpack("<L", self[40:44])[0]
        return datetime.fromtimestamp(timestamp, tz=ZoneInfo("UTC"))

    @property
    def git_sha(self) -> str:
        return bytes(struct.unpack("20B", self[44:64])).hex()

    @property
    def name(self) -> str:
        raw_name: bytes = struct.unpack("52s", self[64:116])[0]
        return raw_name.rstrip(b"\0").decode()

    @property
    def is_valid(self) -> bool:
        return super().is_valid and self.header_version == self.__class__.HEADER_VERSION

    def __str__(self) -> str:
        return f"{self.name} {self.version!s} for HW {self.target_hardware!s}"

    @property
    def description_dict(self) -> Dict[str, Any]:
        return {
            "file name": str(self.file) if self.file is not None else "",
            "application": f"{self.name} {self.version!s}",
            "type": f"{self.__class__.__name__}, {'valid' if self.is_valid else 'invalid'}",
            "checksums": f"Header {self.header_crc:08x}, Image {self.image_crc:08x}",
            "Git SHA": self.git_sha,
            "Build ID": self.build_id,
            "build date": self.build_time_string,
            "header format": f"ImageHeaderV{self.header_version}",
            "address range": f"0x{self.header_start:08x}..0x{self.image_end:08x} ({len(self) / 1024 :.1f} KiB)",
            "vector table": f"0x{self.vector_table:08x}",
            "stack top": f"0x{self.stack_top:08x}",
            "entrypoint": f"0x{self.entrypoint:08x}",
            "up/downgrade": f"{self.min_upgrade_from!s}..{self.max_downgrade_from!s}",
            "hardware": f"{self.min_hardware!s}..{self.target_hardware!s}",
        }

    def is_compatible_to(self, platform: str, release: HardwareVersion) -> bool:
        platform_parts = platform.split(".")
        app_name_parts = self.name.split(".")
        if not app_name_parts[: len(platform_parts)] == platform_parts:
            return False
        if not self.min_hardware <= release <= self.target_hardware:
            return False
        return True


def make_image(
    file: Union[str, Path, None] = None, data: Optional[bytes] = None, base_class: Type[Image] = Image
) -> Image:
    """Factory function to create correct image subclass"""

    if (file is None and data is None) or (file is not None and data is not None):
        raise TypeError("Either file or data has to be specified")

    def find_deepest_subclass(cls: Type[Image], file: Union[str, Path, None], data: Optional[bytes]) -> Image:
        for sub in cls.__subclasses__():
            try:
                return find_deepest_subclass(sub, file, data)
            except ValueError:
                continue
        if file is not None:
            assert data is None
            return cls.from_file(file)
        assert data is not None
        return cls(data)

    return find_deepest_subclass(base_class, file, data)
