import binascii
import struct
from datetime import datetime
from typing import Optional, Type, Union

from .device_id import STM32G4DeviceID
from .versions import HardwareVersion


class DCBBase:
    MAGIC = b"\xa5cfg"
    VERSION: int = ...
    SIZE = 16

    def __init__(self, magic: bytes, version: int, size: int, body: bytes = b"", crc: Optional[int] = None) -> None:
        self.magic = magic
        self.version = version
        self.size = size
        self.body = body
        self._crc = crc

    def validate(self, validate_checksum: bool = True) -> None:
        if self.magic != self.MAGIC:
            raise ValueError(f"magic should be 0x{self.MAGIC.hex()}, got 0x{self.magic.hex()}")
        if self.VERSION is not ... and self.version != self.VERSION:
            raise ValueError(f"version should be {self.VERSION}, got {self.version}")
        if len(self.body) % 4 != 0:
            raise ValueError(f"body has to be word-aligned, got length {len(self.body)}")
        if self.size != DCBBase.SIZE + len(self.body):
            raise ValueError(f"size is supposed to be {self.size}, got {DCBBase.SIZE + len(self.body)}")
        if not validate_checksum:
            return
        crc = self.calculate_crc()
        if crc != self.crc:
            raise ValueError(f"CRC mismatch: expected {self.crc:08x}, got {crc:08x}")

    @property
    def is_valid(self) -> bool:
        try:
            self.validate()
            return True
        except ValueError:
            return False

    @classmethod
    def unpack(cls: Type["DCBBase"], packed_data: bytes) -> "DCBBase":
        magic, version, size = struct.unpack("<4sLL", packed_data[:12])
        body = packed_data[12 : size - 4]
        self = cls(magic, version, size, body)
        self.validate(False)
        (self._crc,) = struct.unpack("<L", packed_data[size - 4 : size])
        return self

    @property
    def crc(self) -> int:
        if self._crc is None:
            self._crc = self.calculate_crc()
        return self._crc

    @property
    def head(self) -> bytes:
        """
        Packs the DCB header into a (Little-Endian) binary representation of the following struct:

        ```c
        struct head {
            uint32_t magic;
            uint32_t version;
            uint32_t size;
        };
        ```

        The head has a fixed length of 12 bytes (3 words).
        """
        return struct.pack("<4sLL", self.magic, self.version, self.size)

    @property
    def tail(self) -> bytes:
        """
        Packs the DCB tail into a (Little-Endian) binary representation of the following struct:

        ```c
        struct tail {
            uint32_t crc;
        };
        ```

        The tail has a fixed length of 4 bytes (1 word).
        """
        return struct.pack("<L", self.crc)

    def calculate_crc(self) -> int:
        """Calculate DCB checksum"""
        return binascii.crc32(self.head + self.body)

    def pack(self) -> bytes:
        """Pack entire DCB into (Little-Endian) binary representation, ready for upload to device."""
        return self.head + self.body + self.tail

    def __repr__(self) -> str:
        args = [self.magic, self.version, self.size]
        kwargs = {"body": self.body}
        if self._crc is not None:
            kwargs["crc"] = self._crc
        argstr = ", ".join(map(repr, args))
        kwargstr = ", ".join(f"{key}={repr(val)}" for key, val in kwargs.items())
        return f"{self.__class__.__name__}({argstr}, {kwargstr})"

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, self.__class__):
            return False
        return self.pack() == __o.pack()

    def __len__(self) -> int:
        return self.SIZE


class DeviceConfigurationV0(DCBBase):
    """
    Python Representation of the following C construct:

    ```c
    struct DeviceConfigurationV0 {
        uint32_t magic;    // should be DEVICE_CONFIG_MAGIC (0x676663a5)
        uint32_t version;  // eDEV_CONFIG_VERSION_0
        uint32_t size;     // sizeof(struct DeviceConfigurationV0) == 72

        char        name[52];  // null-terminated device/platform name, as in uavcan.node.GetInfo.1.0.name
        HWVersion_t release;   // device/platform hardware release
        uint16_t    __pad0;    // padding to align to word boundary, should be zero

        uint32_t crc;  // CRC-32 of the configuration block up to here
    };
    ```
    """

    VERSION = 0
    SIZE = 72

    @classmethod
    def new(
        cls: Type["DeviceConfigurationV0"], name: str, release: Union[HardwareVersion, str]
    ) -> "DeviceConfigurationV0":
        if not isinstance(name, str) or len(name.encode("ascii")) > 50:
            raise ValueError("name has to be a string no longer than 50 ASCII characters")
        if isinstance(release, str):
            release = HardwareVersion(release)
        elif not isinstance(release, HardwareVersion):
            raise TypeError(f"release should be HardwareVersion or str, got {type(release)}")

        body = cls.pack_body(name, release)
        return cls(cls.MAGIC, cls.VERSION, cls.SIZE, body)

    @staticmethod
    def pack_body(name: str, release: HardwareVersion) -> bytes:
        """
        Packs the DCB body into a (Little-Endian) binary representation of the following struct:

        ```c
        struct body {
            char        name[52];
            HWVersion_t release;
            uint16_t    __pad;
        };
        ```

        The body has a fixed length of 56 bytes (14 words).
        """
        body = struct.pack("<52s2sxx", name.encode("ascii"), release.pack())
        assert len(body) == 56
        return body

    @property
    def name(self) -> str:
        bytes_name: bytes = struct.unpack("52s", self.body[0:52])[0]
        return bytes_name.rstrip(b"\0").decode("ascii")

    @property
    def release(self) -> HardwareVersion:
        return HardwareVersion(self.body[52:54])

    def __str__(self) -> str:
        s = f"{self.__class__.__name__}(name={self.name}, release={self.release})"
        if not self.is_valid:
            s += " (INVALID)"
        return s


class DeviceConfigurationV1(DCBBase):
    """
    Python Representation of the following C construct:

    ```c
    struct DeviceConfigurationV1 {
        const uint32_t magic;    // should be DEVICE_CONFIG_MAGIC (0x676663a5)
        const uint32_t version;  // eDEV_CONFIG_VERSION_1
        const uint32_t size;     // sizeof(struct DeviceConfigurationV1) == 104

        const uint32_t    __pad0;    // padding to align next member to double-word boundary
        const char        name[52];  // null-terminated device/platform name, as in uavcan.node.GetInfo.1.0.name
        const HWVersion_t release;   // device/platform hardware release
        const uint16_t    __pad1;    // padding to align to word boundary, should be zero
        const uint64_t    date;      // manufacturing date in microseconds since 1970-01-01T00:00:00Z
        const uint8_t     uid[16];   // 128-bit unique hardware identifier
        const uint32_t    __pad2;    // make crc end align with double-word boundary

        const uint32_t crc;  // CRC-32 of the configuration block up to here
    };
    ```
    """

    VERSION = 1
    SIZE = 104

    @classmethod
    def new(
        cls: Type["DeviceConfigurationV1"],
        name: str,
        release: Union[HardwareVersion, str],
        date: Optional[datetime] = None,
        uid: Optional[bytes] = None,
        stm32_device_id: Union[STM32G4DeviceID, bytes, None] = None,
    ) -> "DeviceConfigurationV1":

        if not isinstance(name, str) or len(name.encode("ascii")) > 50:
            raise ValueError("name has to be a string no longer than 50 ASCII characters")
        if len(name.split(".")) < 3:
            raise ValueError('name has to be of format "com.manufacturer.*"')
        if isinstance(release, str):
            release = HardwareVersion(release)
        elif not isinstance(release, HardwareVersion):
            raise TypeError(f"release should be HardwareVersion or str, got {type(release)}")
        if date is None:
            date = datetime.now()
        elif not isinstance(date, datetime):
            raise TypeError(f"date should be datetime, got {type(date)}")

        if uid is None:
            if isinstance(stm32_device_id, bytes):
                stm32_device_id = STM32G4DeviceID.from_bytes(stm32_device_id)
            elif not isinstance(stm32_device_id, STM32G4DeviceID):
                raise ValueError(f"either uid or stm32_device_id need to be specified")

            name_parts = name.split(".")
            uid_parts = [
                binascii.crc32(".".join(name_parts[:2]).encode("ascii")),
                binascii.crc32(".".join(name_parts[2:]).encode("ascii")),
                int(date.timestamp()),
                binascii.crc32(stm32_device_id.pack()),
            ]
            # big-endian, to support byte-by-byte sorting
            uid = struct.pack(">LLLL", *uid_parts)

        assert len(uid) == 16

        body = cls.pack_body(name, release, date, uid)
        return cls(cls.MAGIC, cls.VERSION, cls.SIZE, body)

    @staticmethod
    def pack_body(name: str, release: HardwareVersion, date: datetime, uid: bytes) -> bytes:
        """
        Packs the DCB body into a (Little-Endian) binary representation of the following struct:

        ```c
        struct body {
            const uint32_t    __pad0;
            const char        name[52];
            const HWVersion_t release;
            const uint16_t    __pad1;
            const uint64_t    date;
            const uint8_t     uid[16];
            const uint32_t    __pad2;
        };
        ```

        The body has a fixed length of 56 bytes (14 words).
        """
        body = struct.pack(
            "<" "xxxx" "52s" "2s" "xx" "Q" "16s" "xxxx",
            name.encode("ascii"),
            release.pack(),
            int(date.timestamp() * 1e6),
            uid,
        )
        assert len(body) == 104 - 16
        return body

    @property
    def name(self) -> str:
        bytes_name: bytes = struct.unpack("52s", self.body[4:56])[0]
        return bytes_name.rstrip(b"\0").decode("ascii")

    @property
    def release(self) -> HardwareVersion:
        return HardwareVersion(self.body[56:58])

    @property
    def date(self) -> datetime:
        timestamp_us: int = struct.unpack("<Q", self.body[60:68])[0]
        return datetime.fromtimestamp(timestamp_us * 1e-6)

    @property
    def date_string(self) -> str:
        return self.date.astimezone().strftime("%Y-%m-%d %H:%M:%S %Z")

    @property
    def uid(self) -> bytes:
        return struct.unpack("16s", self.body[68:84])[0]

    def __str__(self) -> str:
        s = (
            f"{self.__class__.__name__}("
            f"name={self.name}, "
            f"release={self.release}, "
            f"date={self.date_string}, "
            f"uid={self.uid.hex('_', 4)})"
        )
        if not self.is_valid:
            s += " (INVALID)"
        return s


def make_dcb(packed_binary_data: bytes, base_class: Type[DCBBase] = DCBBase) -> DCBBase:
    """Factory function to create correct DCB subclass"""

    def find_deepest_subclass(cls: Type[DCBBase], data: bytes) -> DCBBase:
        for sub in cls.__subclasses__():
            try:
                return find_deepest_subclass(sub, data)
            except (TypeError, ValueError):
                continue
        instance = cls.unpack(data)
        instance.validate(validate_checksum=False)
        return instance

    return find_deepest_subclass(base_class, packed_binary_data)
