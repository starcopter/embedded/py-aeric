import asyncio
import logging
import time
import typing

import pycyphal
import pycyphal.application
import uavcan.time
from pycyphal.transport.redundant import RedundantOutputSession
from uavcan.time import Synchronization_1_0 as Synchronization

_logger = logging.getLogger(__name__)


class TimeSynchronizationMaster:
    def __init__(self, node: pycyphal.application.Node):
        self._node = node
        self._maybe_task: typing.Optional[asyncio.Task[None]] = None
        self._priority = pycyphal.presentation.DEFAULT_PRIORITY
        self._period = float(Synchronization.MAX_PUBLICATION_PERIOD)
        self._publisher = self._node.make_publisher(Synchronization)
        self._started_at = time.monotonic()

        def start() -> None:
            if not self._maybe_task:
                self._started_at = time.monotonic()
                self._maybe_task = asyncio.get_event_loop().create_task(self._task_function())

        def close() -> None:
            if self._maybe_task:
                self._maybe_task.cancel()  # Cancel first to avoid exceptions from being logged from the task.
                self._maybe_task = None

        node.add_lifetime_hooks(start, close)

    @property
    def node(self) -> pycyphal.application.Node:
        return self._node

    @property
    def period(self) -> float:
        """
        How often the Synchronization messages should be published. The upper limit (i.e., the lowest frequency)
        is constrained by the UAVCAN specification; please see the DSDL source of ``uavcan.time.Synchronization``.
        """
        return self._period

    @period.setter
    def period(self, value: float) -> None:
        value = float(value)
        if 0 < value <= Synchronization.MAX_PUBLICATION_PERIOD:
            self._period = value
        else:
            raise ValueError(f"Invalid synchronization period: {value}")

    @property
    def priority(self) -> pycyphal.transport.Priority:
        """
        The transfer priority level to use when publishing Heartbeat messages.
        """
        return self._priority

    @priority.setter
    def priority(self, value: pycyphal.transport.Priority) -> None:
        # noinspection PyArgumentList
        self._priority = pycyphal.transport.Priority(value)

    async def _task_function(self) -> None:
        next_synchronization_at = time.monotonic()
        pub: typing.Optional[pycyphal.presentation.Publisher[Synchronization]] = None
        previous_message_timestamp: pycyphal.transport.Timestamp = pycyphal.transport.Timestamp(0, 0)
        previous_frame_transmission_timestamp_us = uavcan.time.SynchronizedTimestamp_1_0.UNKNOWN

        def feedback_handler(feedback: pycyphal.transport.Feedback) -> None:
            nonlocal previous_frame_transmission_timestamp_us, previous_message_timestamp
            if 0 < feedback.original_transfer_timestamp.monotonic - previous_message_timestamp.monotonic <= 10e-3:
                previous_frame_transmission_timestamp_us = feedback.first_frame_transmission_timestamp.system_ns / 1e3
            else:
                _logger.warning("transmission timestamp lost")
                previous_frame_transmission_timestamp_us = uavcan.time.SynchronizedTimestamp_1_0.UNKNOWN

        try:
            while self._maybe_task:
                try:
                    if self.node.id is not None:
                        if pub is None:
                            pub = self.node.make_publisher(Synchronization)
                        assert pub is not None
                        assert not isinstance(pub.transport_session, RedundantOutputSession)
                        pub.transport_session.enable_feedback(feedback_handler)
                        pub.priority = self._priority
                        message = Synchronization(
                            previous_transmission_timestamp_microsecond=previous_frame_transmission_timestamp_us
                        )
                        previous_message_timestamp = pycyphal.transport.Timestamp.now()
                        if not await pub.publish(message):
                            _logger.warning("%s synchronization send timed out", self)
                except Exception as ex:  # pragma: no cover
                    if (
                        isinstance(ex, (asyncio.CancelledError, pycyphal.transport.ResourceClosedError))
                        or not self._maybe_task
                    ):
                        _logger.debug("%s publisher task will exit: %s", self, ex)
                        break
                    _logger.exception("%s publisher task exception: %s", self, ex)

                next_synchronization_at += self._period
                await asyncio.sleep(next_synchronization_at - time.monotonic())
        finally:
            _logger.debug("%s publisher task is stopping", self)
            if pub is not None:
                pub.close()
