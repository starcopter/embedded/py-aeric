import logging
import os

import uvicorn

from .. import get_logger
from . import app

_logger = get_logger(__name__)

if __name__ == "__main__":
    logging.getLogger("pyric").setLevel(logging.DEBUG)
    try:
        import coloredlogs

        coloredlogs.install(logging.DEBUG)
    except ImportError:
        pass
    logging.getLogger("pycyphal").setLevel(logging.INFO)
    logging.getLogger("pycyphal.application.node_tracker").setLevel(logging.DEBUG)
    logging.getLogger("uavcan.diagnostic.record").setLevel(logging.INFO)
    logging.getLogger("Rx").setLevel(logging.INFO)
    _logger.info("starting as module...")
    _logger.debug("Env: %s", {k: v for k, v in os.environ.items() if "__" in k})
    uvicorn.run(app, host="0.0.0.0", port=8000)
