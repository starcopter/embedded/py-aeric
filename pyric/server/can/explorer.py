"""Node Explorer

Discover nodes on the network and explore their capabilities:
- available registers
- non-standard subjects
"""

from __future__ import annotations

import asyncio
import importlib
import logging
import os
import platform
import re
from dataclasses import dataclass, field
from functools import partial
from typing import Type, TypeVar

import pycyphal.application
import pycyphal.dsdl
import pycyphal.transport
import uavcan.node
import uavcan.time
from influxdb_client import InfluxDBClient, Point, WriteApi, WriteOptions, WritePrecision
from pycyphal.application._node import Subscriber
from pycyphal.application.node_tracker import NodeTracker
from pycyphal.application.register import Natural16
from pycyphal.presentation._port._subscriber import ReceivedMessageHandler

from ... import flatten
from ...cyphal.registry import Register, Registry

_logger = logging.getLogger(__name__)
MessageClass = TypeVar("MessageClass")


@dataclass
class NodeExplorer:
    node: pycyphal.application.Node
    tracker: NodeTracker
    registries: dict[int, Registry] = field(init=False)
    subscriptions: dict[int, AdHocSubscription] = field(init=False)
    lock: asyncio.Lock = field(init=False)
    influx_client: InfluxDBClient = field(init=False)
    influx_writer: WriteApi = field(init=False)
    influx_bucket: str = field(init=False)

    UNREGULATED_SUBJECT_IDS = range(0, 6144)
    NON_STANDARD_SUBJECT_IDS = range(6144, 7168)
    STANDARD_SUBJECT_IDS = range(7168, 8192)

    def __post_init__(self) -> None:
        self.node.registry.setdefault(
            "node_explorer.subject_id_threshold",
            Natural16([max(self.UNREGULATED_SUBJECT_IDS) + 1]),
        )

        self.registries = dict()
        self.subscriptions = dict()
        self.lock = asyncio.Lock()

        self.influx_bucket = os.getenv("INFLUXDB_V2_BUCKET", "pyric")
        self.influx_client: InfluxDBClient = InfluxDBClient.from_env_properties()
        if self.influx_client.default_tags is None:
            self.influx_client.default_tags = {}
        assert isinstance(self.influx_client.default_tags, dict)
        self.influx_client.default_tags |= {
            "logger": f"pyric-{platform.node()}",
            "iface": str(self.node.registry["uavcan.can.iface"]),
        }
        # important: create WriteApi _after_ updating client default tags
        self.influx_writer: WriteApi = self.influx_client.write_api(
            write_options=WriteOptions(batch_size=1000, flush_interval=4000, jitter_interval=2000)
        )
        self.node.add_lifetime_hooks(None, self.influx_client.close)
        self.node.add_lifetime_hooks(None, self.influx_writer.close)

    @property
    def subject_id_threshold(self) -> int:
        """Threshold above which non-fixed subject IDs should be ignored."""
        return int(self.node.registry["node_explorer.subject_id_threshold"])

    @staticmethod
    def _discover_subjects(node_id: int, reg_dict: dict[str, Register]) -> list[DiscoveredSubject]:
        subjects: list[DiscoveredSubject] = list()
        for key in reg_dict:
            if not key.startswith("uavcan.pub.") or not key.endswith(".type"):
                continue
            port_name = key[11:-5]
            assert key == f"uavcan.pub.{port_name}.type"
            if f"uavcan.pub.{port_name}.id" not in reg_dict:
                _logger.warning("%s on node %i has no .id register", port_name, node_id)
                continue
            port_id = reg_dict[f"uavcan.pub.{port_name}.id"].value
            port_type = reg_dict[f"uavcan.pub.{port_name}.type"].value
            subjects.append(DiscoveredSubject(node_id, port_id, port_type, port_name))
        return subjects

    def should_subscribe_to(self, subject: DiscoveredSubject) -> bool:
        return subject.port_id in self.STANDARD_SUBJECT_IDS or subject.port_id < self.subject_id_threshold

    async def explore(self, node_id: int) -> None:
        _logger.debug("Explore node %i", node_id)
        async with self.lock:
            if node_id not in self.registries:
                self.registries[node_id] = Registry(node_id, self.node.make_client)
            else:
                _logger.warning("updating existing registry for node %i", node_id)

            registry = self.registries[node_id]
            await registry.discover_registers()
            static_subjects = [
                DiscoveredSubject(
                    node_id,
                    uavcan.node.Heartbeat_1_0._FIXED_PORT_ID_,
                    "uavcan.node.Heartbeat.1.0",
                    "heartbeat",
                ),
                DiscoveredSubject(
                    node_id,
                    uavcan.time.Synchronization_1_0._FIXED_PORT_ID_,
                    "uavcan.time.Synchronization.1.0",
                    "time_synchronization",
                ),
            ]
            dynamic_subjects = self._discover_subjects(node_id, registry._registers)

            if dynamic_subjects:
                _logger.info(
                    "%i subject(s) discovered for node %i: %s",
                    len(dynamic_subjects),
                    node_id,
                    ", ".join(s.port_name for s in dynamic_subjects),
                )
                importlib.invalidate_caches()

            for sub in set(static_subjects + dynamic_subjects):
                if not self.should_subscribe_to(sub):
                    _logger.info(
                        "skipping port %s of node %i (%i.%s)", sub.port_name, sub.node_id, sub.port_id, sub.port_type
                    )
                    continue
                match = re.match(
                    r"(?P<namespace>[a-zA-Z_][a-zA-Z0-9_]*(\.[a-zA-Z_][a-zA-Z0-9_]*)*)\."
                    r"(?P<shortname>[a-zA-Z_][a-zA-Z0-9_]*)\.(?P<major>\d+)\.(?P<minor>\d+)",
                    sub.port_type,
                )
                if not match:
                    _logger.warning(
                        "port %s of node %i has an invalid data type: %s", sub.port_name, node_id, sub.port_type
                    )
                    continue

                try:
                    namespace = importlib.import_module(match.group("namespace"))
                    # Cyphal specification v1.0-beta, page 43, section 3.8.3.2 "Versioning principles":
                    #
                    #   In order to ensure a deterministic application behavior and ensure a robust migration path as
                    #   data type definitions evolve, all data type definitions that share the same full name and the
                    #   same major version number shall be semantically compatible with each other.
                    #
                    #   An exception to the above rules applies when the major version number is zero. Data type
                    #   definitions bearing the major version number of zero are not subjected to any compatibility
                    #   requirements.
                    #
                    # Therefore, to reach maximum compatibility, we will attempt to load the exact type (major.minor)
                    # for data types with a major version number of zero, and the newest major type for data types
                    # with a major version number greater than zero.
                    py_type_name = "_".join(
                        match.group("shortname", "major")
                        if int(match.group("major")) > 0
                        else match.group("shortname", "major", "minor")
                    )
                    Message: Type[MessageClass] = getattr(namespace, py_type_name)
                except (ImportError, AttributeError):
                    _logger.warning("no datatype for %s found, skipping %s from node %i", sub.port_type, sub, node_id)
                    continue

                if sub.port_id not in self.subscriptions:
                    try:
                        self.subscriptions[sub.port_id] = AdHocSubscription(
                            self.node, sub.port_id, sub.port_type, Message
                        )
                    except ValueError:
                        # most likely an invalid subject ID
                        _logger.debug("not subscribing to %i.%s", sub.port_id, sub.port_type)
                        continue
                    _logger.debug("new AdHocSubscription for %i.%s", sub.port_id, sub.port_type)
                    self.subscriptions[sub.port_id].listen(self._on_discovered_message)

                subscription = self.subscriptions[sub.port_id]

                if subscription.subscriber.dtype is Message:
                    subscription.add_publisher(node_id, sub.port_name)
                    _logger.info(
                        "subscribed to %i.%s from %i nodes %s",
                        sub.port_id,
                        sub.port_type,
                        len(subscription.publishers),
                        set(subscription.publishers.keys()),
                    )
                else:
                    _logger.error(
                        "Conflicting data types for Subject %i: nodes %s publish %s, node %s publishes %s",
                        sub.port_id,
                        set(subscription.publishers.keys()),
                        subscription.subscriber.dtype,
                        node_id,
                        Message,
                    )

    async def forget(self, node_id: int) -> None:
        async with self.lock:
            if node_id not in self.registries:
                return
            del self.registries[node_id]
            sequence_of_port_ids = tuple(self.subscriptions.keys())
            for port_id in sequence_of_port_ids:
                sub = self.subscriptions[port_id]
                sub.remove_publisher(node_id)
                if not sub.has_any_publisher:
                    sub.close()
                    del self.subscriptions[port_id]

    def close(self) -> None:
        for sub in self.subscriptions.values():
            sub.close()

    async def _on_discovered_message(
        self, message: MessageClass, transfer: pycyphal.transport.TransferFrom, port_id: int
    ) -> None:
        node_id = transfer.source_node_id
        sub = self.subscriptions[port_id]
        if node_id not in sub.publishers:
            return
        tags = {"node_id": node_id, "port_id": port_id, "port_name": sub.publishers[node_id]}
        try:
            tags["app_name"] = self.tracker.registry[node_id].info.name.tobytes().decode()
            tags["device_uid"] = self.tracker.registry[node_id].info.unique_id.tobytes().hex()
        except (KeyError, AttributeError):
            pass
        fields = {
            key: (val if not isinstance(val, bool) else int(val))
            for key, val in flatten(pycyphal.dsdl.to_builtin(message)).items()
        }

        data = {"measurement": sub.port_type, "tags": tags, "fields": fields, "time": transfer.timestamp.system_ns}
        point = Point.from_dict(data, WritePrecision.NS)
        self.influx_writer.write(self.influx_bucket, record=point)


@dataclass
class AdHocSubscription:
    node: pycyphal.application.Node
    port_id: int
    port_type: str
    Message: Type[MessageClass]
    subscriber: Subscriber[MessageClass] = field(init=False)
    publishers: dict[int, str] = field(init=False)

    def __post_init__(self) -> None:
        self.subscriber = self.node.presentation.make_subscriber(self.Message, self.port_id)
        self.publishers = dict()

    def add_publisher(self, node_id: int, port_name: str) -> None:
        if node_id in self.publishers:
            _logger.warning("node %i already in publisher list for %i.%s", node_id, self.port_id, self.port_type)
        self.publishers[node_id] = port_name

    def remove_publisher(self, node_id) -> None:
        if node_id in self.publishers:
            del self.publishers[node_id]

    @property
    def has_any_publisher(self) -> bool:
        return bool(self.publishers)

    def listen(self, handler: ReceivedMessageHandler[MessageClass]) -> None:
        self.subscriber.receive_in_background(partial(handler, port_id=self.port_id))

    def close(self) -> None:
        self.subscriber.close()

    def __del__(self) -> None:
        if hasattr(self, "subscriber"):
            self.subscriber.close()


@dataclass(frozen=True)
class DiscoveredSubject:
    node_id: int
    port_id: int
    port_type: str
    port_name: str
