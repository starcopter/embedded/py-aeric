# Copyright (c) 2023 starcopter GmbH
# This software is distributed under the terms of the MIT License.
# Author: Lasse Fröhner <lasse@starcopter.com>

import asyncio
import os
import re
from pathlib import Path
from typing import Optional

import pycyphal.application
import uavcan.diagnostic
import uavcan.node
import uavcan.register
from pycyphal.application.node_tracker import Entry
from pycyphal.application.plug_and_play import CentralizedAllocator

from ... import __version__, cyphal, get_logger
from .explorer import NodeExplorer

_logger = get_logger(__name__)


class AppNode(cyphal.Client):
    NAME = "com.starcopter.pyric.server"

    def __init__(self) -> None:
        self.home = Path(os.getenv("PYRIC_STATE_DIR", ".")).resolve()
        major, minor, _patch = list(map(int, re.match(r"(\d+)\.(\d+)\.(\d+)", __version__).groups()))

        super().__init__(self.NAME, uavcan.node.Version_1_0(major, minor), registry=self.home / "registry.db")

        self.time_server = cyphal.TimeSynchronizationMaster(self.node)

        self.explorer = NodeExplorer(self.node, self.node_tracker)
        self.node.add_lifetime_hooks(None, self.explorer.close)
        self.node_tracker.add_update_handler(self._explore_node_capabilities)

        self.pnp_allocator = CentralizedAllocator(self.node, self.home / "allocation.db")

        self.time_server.period = 0.5

        _logger.info("pyric server started, running in %s", self.home)

    def _explore_node_capabilities(self, node_id: int, old_entry: Optional[Entry], new_entry: Optional[Entry]) -> None:
        appeared = old_entry is None and new_entry is not None and new_entry.info is None
        disappeared = new_entry is None
        restarted = old_entry is not None and new_entry is not None and new_entry.info is None
        _new_info = old_entry is not None and new_entry is not None and new_entry.info is not None

        async def tasks_in_order():
            if disappeared or restarted:
                await self.explorer.forget(node_id)
            if appeared or restarted:
                await self.explorer.explore(node_id)

        asyncio.create_task(tasks_in_order())
