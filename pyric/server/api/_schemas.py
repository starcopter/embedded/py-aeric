import collections.abc
import re
from typing import Any, Dict, Optional, Union

import numpy as np
from numpy.typing import NDArray
from pydantic import BaseModel, validator

from ...cyphal.registry import NativeValue


class BaseSchema(BaseModel):
    class Config:
        orm_mode = True


class Health(BaseSchema):
    """
    Abstract component health information. If the node performs multiple activities (provides multiple network services),
    its health status should reflect the status of the worst-performing activity (network service).
    """

    value: int


class Mode(BaseSchema):
    """
    The operating mode of a node.
    Reserved values can be used in future revisions of the specification.
    """

    value: int


class Version(BaseSchema):
    """
    A shortened semantic version representation: only major and minor.
    The protocol generally does not concern itself with the patch version.
    """

    major: int
    minor: int


class GetInfoResponse(BaseSchema):
    protocol_version: Version
    hardware_version: Version
    software_version: Version
    software_vcs_revision_id: str
    unique_id: str
    name: str
    software_image_crc: str = ""
    certificate_of_authenticity: str = ""

    @validator("software_vcs_revision_id", pre=True)
    def _parse_git_hash(cls, val: Union[int, str]) -> str:
        if isinstance(val, str):
            return val
        return format(val, "016x" if val > 0xFFFFFFFF else "08x")

    @validator("unique_id", "certificate_of_authenticity", pre=True)
    def _parse_uid(cls, val: Union[NDArray[np.uint8], str]) -> str:
        if isinstance(val, str):
            return val
        return val.tobytes().hex()

    @validator("name", pre=True)
    def _parse_uavcan_string(cls, val: Union[NDArray[np.uint8], str]) -> str:
        if isinstance(val, str):
            return val
        return val.tobytes().decode()

    @validator("software_image_crc", pre=True)
    def _parse_crc(cls, val: Union[NDArray[np.uint64], str]) -> str:
        if isinstance(val, str):
            return val
        if len(val) < 1:
            return ""
        return cls._parse_git_hash(val[0])


class Heartbeat(BaseSchema):
    """
    Abstract node status information.
    This is the only high-level function that shall be implemented by all nodes.

    All UAVCAN nodes that have a node-ID are required to publish this message to its fixed subject periodically.
    Nodes that do not have a node-ID (also known as "anonymous nodes") shall not publish to this subject.

    The default subject-ID 7509 is 1110101010101 in binary. The alternating bit pattern at the end helps transceiver
    synchronization (e.g., on CAN-based networks) and on some transports permits automatic bit rate detection.

    Network-wide health monitoring can be implemented by subscribing to the fixed subject.
    """

    uptime: int
    health: Health
    mode: Mode
    vendor_specific_status_code: int


class Register(BaseSchema):
    _ARRAY_BASE_TYPES = {
        "bit": bool,
        "integer64": int,
        "integer32": int,
        "integer16": int,
        "integer8": int,
        "natural64": int,
        "natural32": int,
        "natural16": int,
        "natural8": int,
        "real64": float,
        "real32": float,
        "real16": float,
    }

    dtype: str
    value: NativeValue
    min: NativeValue = None
    max: NativeValue = None
    default: NativeValue = None
    mutable: Optional[bool] = None
    persistent: Optional[bool] = None
    timestamp: float = 0.0

    @validator("value", "min", "max", "default")
    def _validate_native_value(cls, value, values: Dict[str, Any]) -> NativeValue:
        dtype = values["dtype"]
        if dtype == "empty" or value is None:
            return None
        if dtype == "string":
            if isinstance(value, (bytes, bytearray)):
                return bytes(value).decode("utf8", errors="replace")
            return str(value)
        if dtype == "unstructured":
            return "0x" + bytes(value).hex()
        match = re.match(r"([a-z0-9]+)\[(\d+)\]", dtype)
        assert match is not None
        base_type = match.group(1)
        length = int(match.group(2))
        func = cls._ARRAY_BASE_TYPES[base_type]
        value_is_sequence = isinstance(value, collections.abc.Sequence)
        if length == 1:
            if value_is_sequence:
                # unpack
                assert len(value) == 1
                return func(value[0])
            return func(value)
        assert value_is_sequence
        return [func(v) for v in value]


class NodeInfo(BaseSchema):
    heartbeat: Heartbeat
    info: Optional[GetInfoResponse] = None
