# Copyright (c) 2023 starcopter GmbH
# This software is distributed under the terms of the MIT License.
# Author: Lasse Fröhner <lasse@starcopter.com>

from ._app import app
