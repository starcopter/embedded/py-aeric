from __future__ import annotations

import asyncio
import os
import string
import time
from collections.abc import Mapping, Sequence
from typing import Any, Union

from fastapi import Body, Depends, FastAPI, File, HTTPException, Request, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import HTMLResponse, ORJSONResponse, PlainTextResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from ...types import make_image
from ...types.image import AppImage
from ..can import AppNode
from ._schemas import NativeValue, NodeInfo, Register

app = FastAPI(default_response_class=ORJSONResponse)
node = AppNode()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")


def format_uptime(value: int) -> str:
    seconds = value
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)
    weeks, days = divmod(days, 7)

    uptime_str = f"{hours:02d}:{minutes:02d}:{seconds:02d}"
    if days or weeks:
        uptime_str = f"{days}d {uptime_str}"
    if weeks:
        uptime_str = f"{weeks}w {uptime_str}"

    return uptime_str


def format_hardware_version(value: NodeInfo) -> str:
    major = value.info.hardware_version.major
    minor = value.info.hardware_version.minor
    if value.info.name.startswith("com.starcopter") and 1 <= major <= len(string.ascii_uppercase):
        major = string.ascii_uppercase[major - 1]
    return f"{major}.{minor}"


def c_format(value: Union[Mapping[str, Any], Sequence[Any], Any]) -> str:
    if isinstance(value, (int, float)):
        return str(value)
    if isinstance(value, str):
        if len(value) == 1:
            # assume char, not char[]
            return f"'{value}'"
        return f'"{value}"'
    if isinstance(value, Sequence):
        return "{" + ", ".join(c_format(val) for val in value) + "}"
    if isinstance(value, Mapping):
        return "{" + ", ".join(f".{key} = {c_format(val)}" for key, val in value.items()) + "}"
    raise TypeError(f"Incompatible type: {type(value)}")


templates.env.filters["format_uptime"] = format_uptime
templates.env.filters["format_hardware_version"] = format_hardware_version
templates.env.filters["c_format"] = c_format

templates.env.autoescape = False


@app.on_event("startup")
async def start_uavcan_node():
    # this will create the pycyphal.application.Node
    node.start()


@app.on_event("shutdown")
def stop_uavcan_node():
    node.close()


@app.get("/", response_class=RedirectResponse)
async def root():
    return "/ui/"


def find_nodes(
    uid: str = None,
    name: str = None,
    prefix: str = None,
    node_id: int = None,
    mode: int = None,
    health: int = None,
    vcs: str = None,
    crc: str = None,
) -> dict[int, NodeInfo]:
    def matches(nid: int, ninfo: NodeInfo) -> bool:
        if not (
            (node_id is None or node_id == nid)
            and (mode is None or ninfo.heartbeat.mode.value == mode)
            and (health is None or ninfo.heartbeat.health.value == health)
        ):
            return False

        if ninfo.info is None:
            return uid is None and name is None and prefix is None and vcs is None and crc is None

        return (
            (uid is None or ninfo.info.unique_id == uid)
            and (name is None or ninfo.info.name == name)
            and (prefix is None or ninfo.info.name.startswith(prefix))
            and (
                vcs is None
                or ninfo.info.software_vcs_revision_id.startswith(vcs[: len(ninfo.info.software_vcs_revision_id)])
            )
            and (crc is None or ninfo.info.software_image_crc == crc)
        )

    node_generator = ((nid, NodeInfo.from_orm(entry)) for nid, entry in node.node_tracker.registry.items())

    return {nid: ninfo for nid, ninfo in node_generator if matches(nid, ninfo)}


@app.get("/ui/", response_class=HTMLResponse)
async def get_ui_overview(request: Request, nodes: dict[int, NodeInfo] = Depends(find_nodes, use_cache=False)):
    return templates.TemplateResponse("nodes.html", {"request": request, "nodes": nodes})


@app.get("/ui/node/{id}", response_class=HTMLResponse)
async def get_ui_node_details(request: Request, id: int, refresh: bool = False):
    try:
        node_info = NodeInfo.from_orm(node.node_tracker.registry[id])
    except KeyError as e:
        raise HTTPException(404, f"Node '{id}' not found") from e

    registers = await read_multiple_registers(id, refresh)

    return templates.TemplateResponse(
        "registers.html", {"request": request, "id": id, "node": node_info, "registers": registers}
    )


@app.get("/header.c", response_class=PlainTextResponse)
async def get_rendered_header_template(request: Request):
    return templates.TemplateResponse(
        "header.c.j2",
        {
            "request": request,
            "build_type": 0,
            "version": {"major": 0, "minor": 3, "patch": 0, "pre_type": 8, "pre_num": 2},
            "min_upgrade_from": {"major": 0, "minor": 0, "patch": 0, "pre_type": 15, "pre_num": 0},
            "max_downgrade_from": {"major": 0, "minor": 3, "patch": 0, "pre_type": 15, "pre_num": 0},
            "hardware_max": {"major": "C", "minor": 4},
            "hardware_min": {"major": "B", "minor": 1},
            "timestamp": int(time.time()),
            "git_sha": list(range(20)),
            "name": "com.starcopter.pyric.header",
        },
        media_type="text/plain",
    )


@app.get("/nodes/", response_model=dict[str, NodeInfo])
def get_nodes(nodes: dict[int, NodeInfo] = Depends(find_nodes, use_cache=False)):
    if not nodes:
        raise HTTPException(404, "No Nodes match the query")
    return nodes


@app.post("/nodes/reboot", response_model=dict[str, dict[str, str]])
async def reboot_nodes(
    nodes: dict[int, NodeInfo] = Depends(find_nodes, use_cache=False), timeout: float = 1.0
) -> dict[int, dict[str, str]]:
    if not nodes:
        raise HTTPException(404, "No Nodes match the query")

    tasks = [asyncio.create_task(node.restart_node(nid, wait=True, timeout=timeout)) for nid in nodes]
    results = await asyncio.gather(*tasks, return_exceptions=True)

    return {
        nid: {
            "unique_id": ninfo.info.unique_id,
            "name": ninfo.info.name,
            "result": "success" if isinstance(result, float) else f"Error: {result!s}",
            "duration": result if isinstance(result, float) else None,
        }
        for (nid, ninfo), result in zip(nodes.items(), results)
    }


@app.post("/nodes/update", response_model=dict[str, dict[str, Any]])
async def update_nodes(
    nodes: dict[int, NodeInfo] = Depends(find_nodes, use_cache=False),
    file: UploadFile = File(...),
    timeout: float = 5.0,
):
    if not nodes:
        raise HTTPException(404, "No Nodes match the query")

    image_bytes = bytes(await file.read())
    try:
        image = make_image(data=image_bytes, base_class=AppImage)
    except ValueError:
        raise HTTPException(422, f"{file.filename} is not a valid ApplicationImage")

    assert isinstance(image, AppImage)
    assert image.build_id != ""
    assert image.name != ""

    image.file = node.file_server.roots[0] / f"{image.name}.{image.build_id[:8]}.bin"
    image.commit()
    os.sync()

    tasks = [asyncio.create_task(node.update(nid, image, wait=True, timeout=timeout)) for nid in nodes]
    results = await asyncio.gather(*tasks, return_exceptions=True)

    image_info = image.description_dict
    image_info["file name"] = str(image.file.name)

    return {
        "image": image_info,
        "status": {
            str(nid): {
                "unique_id": ninfo.info.unique_id,
                "name": ninfo.info.name,
                "result": "success" if isinstance(result, float) else f"Error: {result!s}",
                "duration": result if isinstance(result, float) else None,
            }
            for (nid, ninfo), result in zip(nodes.items(), results)
        },
    }


@app.get("/registers/", response_model=dict[str, list[str]])
def list_registers(nodes: dict[int, NodeInfo] = Depends(find_nodes, use_cache=False)):
    if not nodes:
        raise HTTPException(404, "No Nodes match the query")
    return {
        nid: sorted([reg.name for reg in registry])
        for nid, registry in node.explorer.registries.items()
        if nid in nodes
    }


@app.get("/registers/{nid}", response_model=dict[str, Register])
async def read_multiple_registers(nid: int, refresh: bool = False) -> dict[str, Register]:
    try:
        registry = node.explorer.registries[nid]
    except KeyError as e:
        raise HTTPException(404, f"Node '{nid}' not found") from e

    if refresh:
        tasks = [asyncio.create_task(registry.refresh_register(reg.name)) for reg in registry]
        # TODO: handle timeout
        await asyncio.gather(*tasks)

    return {reg.name: Register.from_orm(reg) for reg in registry}


@app.post("/registers/{nid}", response_model=dict[str, Register])
async def write_multiple_registers(nid: int, values: dict[str, Union[Register, NativeValue]]) -> dict[str, Register]:
    try:
        registry = node.explorer.registries[nid]
    except KeyError as e:
        raise HTTPException(404, f"node {nid} not found") from e
    extra = [name for name in values if name not in registry]
    if extra:
        raise HTTPException(422, f"the following values were not found in {nid}'s registry: {', '.join(extra)}")
    assert all(name in registry for name in values)

    for name, reg in values.items():
        if not registry[name].mutable:
            continue
        value = reg.value if isinstance(reg, Register) else reg
        await registry.set_value(name, value)

    return read_multiple_registers(nid)


@app.get("/registers/{nid}/{name}", response_model=Register)
async def read_single_register(nid: int, name: str, refresh: bool = False) -> Register:
    try:
        registry = node.explorer.registries[nid]
        register = registry[name]
    except KeyError as e:
        raise HTTPException(404, f"Register '{name}' not found for node {nid}") from e
    if refresh:
        # TODO: handle timeout
        await registry.refresh_register(name)
    return Register.from_orm(register)


@app.post("/registers/{nid}/{name}", response_model=Register)
async def write_single_register(
    nid: int, name: str, value: Union[Register, NativeValue] = Body(..., embed=True)
) -> Register:
    try:
        registry = node.explorer.registries[nid]
        register = registry[name]
    except KeyError as e:
        raise HTTPException(404, f"Register '{name}' not found for node {nid}") from e

    try:
        native_value = value.value if isinstance(value, Register) else value
        await registry.set_value(name, native_value)
    except (ValueError) as e:
        raise HTTPException(422, f"converting {native_value} to {register.dtype} failed: {e}") from e
    return Register.from_orm(register)
