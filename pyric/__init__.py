# Copyright (c) 2023 starcopter GmbH
# This software is distributed under the terms of the MIT License.
# Author: Lasse Fröhner <lasse@starcopter.com>

import importlib.metadata
import os
import pathlib
import sys

__module_root__: pathlib.Path = pathlib.Path(__file__).parent.resolve()
__metadata__ = importlib.metadata.metadata("sc-pyric")
__version__ = __metadata__.get("version")
__author__ = __metadata__.get("author")
__email__ = __metadata__.get("author-email")
__copyright__ = f"Copyright (c) 2023 {__author__} <{__email__}>"
__license__ = __metadata__.get("license")

from ._util import flatten, get_logger
