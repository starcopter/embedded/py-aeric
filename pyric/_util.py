# Copyright (c) 2023 starcopter GmbH
# This software is distributed under the terms of the MIT License.
# Author: Lasse Fröhner <lasse@starcopter.com>

from __future__ import annotations

import logging
from collections.abc import Collection, Mapping
from typing import Any


def get_logger(name: str) -> logging.Logger:
    """
    This is a trivial wrapper over :func:`logging.getLogger` that removes private components from the logger name.
    For example, ``pyric.cmd.file_server._cmd`` becomes ``pyric.cmd.file_server`` (private submodule hidden).
    """
    return logging.getLogger(name.replace("__", "").split("._", 1)[0])


def flatten(d: dict[Any, Any] | Collection[Any], parent_key: str = "", sep: str = ".") -> dict[str, Any]:
    def add_item(items: list[tuple[str, Any]], new_key: str, v: Mapping[Any, Any] | Collection[Any]) -> None:
        if isinstance(v, Mapping) or (isinstance(v, Collection) and not isinstance(v, str)):
            for_extension = flatten(v, new_key, sep)
            if for_extension is not None:
                # noinspection PyTypeChecker
                items.extend(for_extension.items())
        else:
            items.append((new_key, v))

    if isinstance(d, Mapping):
        items: list[tuple[str, Any]] = []
        if len(d) == 1:
            ((k, v),) = d.items()
            new_key = parent_key or str(k)
            add_item(items, new_key, v)
        else:
            for k, v in d.items():
                new_key = parent_key + sep + str(k) if parent_key else str(k)
                add_item(items, new_key, v)
        return dict(items)

    if isinstance(d, Collection) and not isinstance(d, str):
        items = []
        for i, v in enumerate(d):
            new_key = parent_key + f"[{i}]" if parent_key else str(f"[{i}]")
            add_item(items, new_key, v)
        return dict(items)

    return {}
